#!/usr/bin/env bash
# Usage: build.sh [<build-type>]
#
# If the build type (second arg. `<build-type>`) is `prod`
# the images are build in `release` mode. For all other build types
# the images are build for development and testing purposes
# By default the build type is `prod`.

set -eu
set -o pipefail

ROOT_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)/.."
cd "$ROOT_DIR"

function ci_running() {
  [ "${CI:-}" = "true" ] && return 0
  return 1
}

# Define the image tag depending on the context.
function get_image_tag() {
  local build_type="$1"
  local image_tag="${build_type}-latest"

  # Define image tag.
  if ci_running; then
    # The image tag gets adjusted depending on
    # if it is a merge request or build on the
    # main branch or on a tag.

    image_tag="${CI_COMMIT_REF_SLUG:-}"

    if [[ -n "${CI_COMMIT_TAG:-}" ]]; then
      image_tag="${CI_COMMIT_TAG}"
    elif [[ -n "${CI_OPEN_MERGE_REQUESTS:-}" ]]; then
      image_tag=mr$(echo "${CI_OPEN_MERGE_REQUESTS}" | cut -d, -f -1 | cut -d\! -f 2)
    elif [[ "${CI_COMMIT_BRANCH:-}" == "main" ]]; then
      image_tag="latest"
    fi
  fi

  echo "$image_tag"
}

function main() {
  # Define common build variables.
  local container_mgr=${CI_CONTAINER_MGR:-buildah}
  local project_dir=${CI_PROJECT_DIR:-.}
  local registry_name=${CI_REGISTRY_IMAGE:-"containers-storage:pipglr"}
  local build_type=${CI_BUILD_TYPE:-${1:-prod}}

  # Define image name and tag.
  local image_tag image_name
  image_tag=$(get_image_tag "$build_type")
  image_name="${registry_name}:${image_tag}"

  # Define OpenContainers labels.
  local oc_project_url=${CI_PROJECT_URL:-file://$ROOT_DIR}
  local oc_commit_sha=${CI_COMMIT_SHA:-$(git rev-parse HEAD)}
  local oc_job_started_at=${CI_JOB_STARTED_AT:-$(date -u --iso-8601=seconds)}
  local oc_version="${image_tag}"

  BUILD_CMD=(
    "${container_mgr}" build
    --label "org.opencontainers.image.source=${oc_project_url}"
    --label "org.opencontainers.image.revision=${oc_commit_sha}"
    --label "org.opencontainers.image.created=${oc_job_started_at}"
    --label "org.opencontainers.image.version=${oc_version}"
    --build-arg "BUILD_TYPE=${build_type}"
    -t "$image_name"
    "${project_dir}")

  echo "Build image: '$image_name'"
  echo -e "Build command:\n" "${BUILD_CMD[@]}"

  "${BUILD_CMD[@]}"

  echo "Images are:"
  "${container_mgr}" images

  if ci_running; then
    echo "Pushing image: ${image_name}"
    "${container_mgr}" push "${image_name}"
  fi
}

main "$@"
